<?php if (!defined('THINK_PATH')) exit();?><!doctype html>
<html>
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<title>系统后台 - Tpcms内容管理系统 - by Tpcms</title>
<?php if(CONTROLLER_NAME == "Login"): ?><link rel="stylesheet" type="text/css" href="/dev/Data/Public/admin/css/admin_login.css"  />
<link rel="stylesheet" type="text/css" href="/dev/Data/Public/admin/css/admin_default_color.css" />
<?php else: ?>
<link href="/dev/Data/Public/admin/css/admin_style.css" rel="stylesheet" />
<link href="/dev/Data/Public/org/artDialog/skins/default.css" rel="stylesheet" /><?php endif; ?>

<script type='text/javascript'>
MODULE='/dev/index.php/Admin'; //当前模块
CONTROLLER='/dev/index.php/Admin/Article'; //当前控制器)
ACTION='/dev/index.php/Admin/Article/welcome';//当前方法(方法)
ROOT='/dev'; //当前项目根路径
PUBLIC= '/dev/Data/Public/admin';//当前定义的Public目录
</script>
<script src="/dev/Data/Public/org/wind.js"></script>
<script src="/dev/Data/Public/org/jquery.js"></script>
</head>
<body class="J_scroll_fixed">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="180">
    <iframe name="left" id="iframe_categorys" src="<?php echo U('Article/category');?>" style="height: 100%; width: 180px;"  frameborder="0" scrolling="auto"></iframe></td>
    <td width="3" bgcolor="#CCCCCC">
    </td>
    <td>
    <iframe name="right" id="iframe_categorys_list" src="<?php echo U('Index/copyright');?>"   style="height: 100%; width:100%;border:none;"   frameborder="0" scrolling="auto"></iframe></td>
  </tr>
</table>
<script type="text/javascript">
var B_frame_height = parent.$("#B_frame").height()-8;
$(window).on('resize', function () {
    setTimeout(function () {
		B_frame_height = parent.$("#B_frame").height()-8;
        frameheight();
    }, 100);
});
function frameheight(){
	$("#iframe_categorys").height(B_frame_height);
	$("#iframe_categorys_list").height(B_frame_height);
}
(function (){
	frameheight();
})();
</script>
</body>
</html>