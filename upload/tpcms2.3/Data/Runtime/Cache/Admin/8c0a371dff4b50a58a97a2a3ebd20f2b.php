<?php if (!defined('THINK_PATH')) exit();?><!doctype html>
<html>
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<title>系统后台 - Tpcms内容管理系统 - by Tpcms</title>
<?php if(CONTROLLER_NAME == "Login"): ?><link rel="stylesheet" type="text/css" href="/dev/Data/Public/admin/css/admin_login.css"  />
<link rel="stylesheet" type="text/css" href="/dev/Data/Public/admin/css/admin_default_color.css" />
<?php else: ?>
<link href="/dev/Data/Public/admin/css/admin_style.css" rel="stylesheet" />
<link href="/dev/Data/Public/org/artDialog/skins/default.css" rel="stylesheet" /><?php endif; ?>

<script type='text/javascript'>
MODULE='/dev/index.php/Admin'; //当前模块
CONTROLLER='/dev/index.php/Admin/Ucenter'; //当前控制器)
ACTION='/dev/index.php/Admin/Ucenter/index';//当前方法(方法)
ROOT='/dev'; //当前项目根路径
PUBLIC= '/dev/Data/Public/admin';//当前定义的Public目录
</script>
<script src="/dev/Data/Public/org/wind.js"></script>
<script src="/dev/Data/Public/org/jquery.js"></script>
</head>
<body class="J_scroll_fixed">
<div class="wrap J_check_wrap">
    <div class="nav">
        <ul class="cc">

            <li class="current"><a href="javascsript:;">Ucenter配置</a></li>

        </ul>
    </div>
    <div class="h_a">Ucserver配置</div>
    <form action="<?php echo U('setucenter');?>" method="post" class="J_ajaxForm" >
        <div class="table_full">
            <table width="100%"  class="table_form">
               <tr>
                    <th width="200">
                        数据库服务器
                    </th>
                    <td><input type="text" name='dbhost' class='input' size='50' value="<?php echo ($ucenter["dbhost"]); ?>"/></td>
                </tr>
                <tr>
                    <th>
                        数据库用户名
                    </th>
                    <td><input type="text" name='dbuser' class='input' size='50' value="<?php echo ($ucenter["dbuser"]); ?>"/></td>
                </tr>
                <tr>
                    <th>
                       数据库密码
                    </th>
                    <td><input type="text" name='dbpw' class='input' size='50' value="<?php echo ($ucenter["dbpw"]); ?>"/></td>
                </tr>
                <tr>
                    <th>
                        数据库名
                    </th>
                    <td><input type="text" name='dbname' class='input' size='50' value="<?php echo ($ucenter["dbname"]); ?>"/></td>
                </tr>
                <tr>
                    <th>
                        表名前缀
                    </th>
                    <td><input type="text" name='tablepre' class='input' size='50' value="<?php echo ($ucenter["tablepre"]); ?>"/></td>
                </tr>
                <tr>
                    <th>
                        通信密钥
                    </th>
                    <td><input type="text" name='uc_key' class='input' size='50' value="<?php echo ($ucenter["uc_key"]); ?>"/></td>
                </tr>
                <tr>
                    <th>
                        Ucserverd地址
                    </th>
                    <td><input type="text" name='uc_api' class='input' size='50' value="<?php echo ($ucenter["uc_api"]); ?>"/></td>
                </tr>
            </table>
        </div>
        <div class="">
            <div class="btn_wrap_pd">
                <button class="btn btn_submit mr10 " type="submit">保存</button>

            </div>
        </div>
    </form>
</div>
<script type="text/javascript" src="/dev/Data/Public/admin/js/mod.common.js"></script>
</body>
</html>