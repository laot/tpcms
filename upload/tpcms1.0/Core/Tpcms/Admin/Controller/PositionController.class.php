<?php
/** [广告位置]
 * @Author: 976123967@qq.com
 * @Date:   2015-04-15 10:45:58
 * @Last Modified by:   happy
 * @Last Modified time: 2015-05-01 19:19:11
 */
namespace Admin\Controller;
class PositionController extends PublicController{
	/**
	 * [index ]
	 * @return [type] [description]
	 */
	public function index()
	{
		$data = $this->logic->get_all();
		$this->assign('data',$data);
		$this->display();
	}
}