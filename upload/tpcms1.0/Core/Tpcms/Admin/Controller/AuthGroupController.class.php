<?php
/**[用户组控制器]
 * @Author: chenli
 * @Email:  976123967@qq.com
 * @Date:   2015-02-23 11:53:21
 * @Last Modified by:   happy
 * @Last Modified time: 2015-05-01 19:16:19
 */
namespace Admin\Controller;
class AuthGroupController extends PublicController{


	public function rule()
	{
		if(IS_POST)
		{
			if(!$this->logic->alter_rule())
				$this->error($this->model->getError());
			$this->success('规则设置成功');
			die;
		}
		$rule = D('AuthRule','Logic')->get_all(1);
		$this->assign('rule',$rule);

		$field  = $this->logic->get_one(I('get.id'));
		$this->assign('field',$field);
		$this->display();
	}
}
