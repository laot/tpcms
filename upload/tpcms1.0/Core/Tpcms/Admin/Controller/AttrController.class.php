<?php
/**[属性管理]
 * @Author: 976123967@qq.com
 * @Date:   2015-04-28 15:29:46
 * @Last Modified by:   happy
 * @Last Modified time: 2015-05-01 19:16:19
 */
namespace Admin\Controller;
class AttrController extends PublicController{


	public function _initialize()
	{
		parent::_initialize();
		$type = D('Type','Logic')->get_all();
		$this->assign('type',$type);
		$typename = $type[I('get.typeid')]['typename'];
		$this->assign('typename',$typename);
	}

	public function index()
	{
		$typeid = I('get.typeid');
		$data = $this->logic->get_all($typeid);
		$this->assign('data',$data);
		$this->display();
	}


	



}