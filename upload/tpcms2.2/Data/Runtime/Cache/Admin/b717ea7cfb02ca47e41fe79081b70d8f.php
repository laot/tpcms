<?php if (!defined('THINK_PATH')) exit();?>
<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<title><?php echo (C("cfg_name")); ?> - 提示信息</title>
<link href="/test/tpcms/Data/Public/admin/css/admin_style.css" rel="stylesheet" />
<link href="/test/tpcms/Data/Public/org/artDialog/skins/default.css" rel="stylesheet" />

<script type='text/javascript'>
MODULE='/test/tpcms/index.php/Admin'; //当前模块
CONTROLLER='/test/tpcms/index.php/Admin/Cache'; //当前控制器)
ACTION='/test/tpcms/index.php/Admin/Cache/update';//当前方法(方法)
ROOT='/test/tpcms'; //当前项目根路径
PUBLIC= '/test/tpcms/Data/Public/admin';//当前定义的Public目录
</script>
<script src="/test/tpcms/Data/Public/org/wind.js"></script>
<script src="/test/tpcms/Data/Public/org/jquery.js"></script>
</head>
<body>
<div class="wrap">
  <div id="error_tips">
    <h2><?php echo ($msgTitle); ?></h2>
    <div class="error_cont">
      <ul>
        <li><?php echo ($message); ?></li>
      </ul>
      <div class="error_return"><a href="<?php echo ($jumpUrl); ?>" class="btn">返回</a></div>
    </div>
  </div>
</div>
<script src="/test/tpcms/Data/Public/admin/mod.common.js"></script>
<script language="javascript">
setTimeout(function(){
	location.href = '<?php echo ($jumpUrl); ?>';
},<?php echo ($waitSecond); ?>*1000);
</script>
</body>
</html>